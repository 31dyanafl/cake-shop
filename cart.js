const items=[
    {
        "id":1,
        "name":"Light Rose",
        "cost":449
    },
    {
        "id":2,
        "name":"Flower Hat",
        "cost":649
    },
    {
        "id":3,
        "name":"Dripping Chocolate",
        "cost":549
    },
    {
        "id":4,
        "name":"Warm Rush",
        "cost":399
    },
    {
        "id":5,
        "name":"Cascade",
        "cost":549

    },
    {
        "id":6,
        "name":"Purple Princess",
        "cost":649 
    },
    {
        "id":7,
        "name":"Hello Kitty",
        "cost":1699
    },
    {
        "id":8,
        "name":"Make-it-up",
        "cost":1899
    },
    {
        "id":9,
        "name":"Pink Smile",
        "cost":1499
    },
    {
        "id":10,
        "name":"Half Rose",
        "cost":449
    },
    {
        "id":11,
        "name":"Chocolate Coat",
        "cost":599
    },
    {
        "id":12,
        "name":"Sun,moon and stars",
        "cost":1449
    }
];
var TotalAmount = 0;
var ItemCount= 0;

function AddToCart(id)
{
    var itemid=id;
    var item=items.find(item => item.id === itemid);
    console.log(item);
    var itemcost= item.cost;
    var itemname = item.name;
    var quantity = 1;
    ItemCount+= 1;
    TotalAmount+= quantity * itemcost;

    var cart= document.getElementById("carttable");
    cart.innerHTML +=
    `
    <tr>
        <th>${ItemCount}</th>
        <th>${itemname}</th>
        <th>${quantity}</th>
        <th>${itemcost}</th>
        <th>${quantity * itemcost}</th>
    </tr>
    `

    var cartTotal = document.getElementById("cartTotal");
    cartTotal.innerHTML =
    `
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <th>TOTAL</th>
        <th>${TotalAmount}</th>
    </tr>

    `

        
}